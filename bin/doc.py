# -*- coding: utf-8 -*-
#!/usr/bin/env python
import os
import sys
import shutil
from gi.repository import Gtk

folderxpm = [
    "17 16 7 1",
    "  c #000000",
    ". c #808000",
    "X c yellow",
    "o c #808080",
    "O c #c0c0c0",
    "+ c white",
    "@ c None",
    "@@@@@@@@@@@@@@@@@",
    "@@@@@@@@@@@@@@@@@",
    "@@+XXXX.@@@@@@@@@",
    "@+OOOOOO.@@@@@@@@",
    "@+OXOXOXOXOXOXO. ",
    "@+XOXOXOXOXOXOX. ",
    "@+OXOXOXOXOXOXO. ",
    "@+XOXOXOXOXOXOX. ",
    "@+OXOXOXOXOXOXO. ",
    "@+XOXOXOXOXOXOX. ",
    "@+OXOXOXOXOXOXO. ",
    "@+XOXOXOXOXOXOX. ",
    "@+OOOOOOOOOOOOO. ",
    "@                ",
    "@@@@@@@@@@@@@@@@@",
    "@@@@@@@@@@@@@@@@@"
    ]
folderpb = gtk.gdk.pixbuf_new_from_xpm_data(folderxpm)

filexpm = [
    "12 12 3 1",
    "  c #000000",
    ". c #ffff04",
    "X c #b2c0dc",
    "X        XXX",
    "X ...... XXX",
    "X ......   X",
    "X .    ... X",
    "X ........ X",
    "X .   .... X",
    "X ........ X",
    "X .     .. X",
    "X ........ X",
    "X .     .. X",
    "X ........ X",
    "X          X"
    ]
filepb = gtk.gdk.pixbuf_new_from_xpm_data(filexpm)

column_names = ['Name', 'Size', 'Mode', 'Last Changed']

def prepare_maindir(dirname):
    if not os.path.isdir(dirname):
        os.makedirs(dirname)
    if not os.path.isfile(dirname): 
        shutil.copy("../test/index.dw", dirname)
    return None

class dwkMain:
    """This is the main class for dwk entry point"""
    
    def __init__(self):
        
        self.home = os.path.expanduser("~/dwk")
        prepare_maindir(self.home)
        self.gladefile = "dwk.glade"  
        self.builder = Gtk.Builder()
        self.builder.add_from_file(self.gladefile)
        self.builder.connect_signals(Handler())
        self.window = self.builder.get_object("window1")
        self.textview1 = self.builder.get_object("textview1")
        textbuffer = self.textview1.get_buffer()
        tag = textbuffer.create_tag("diogoduarte", background="orange")
        #textbuffer.apply_tag(tag, start_iter, end_iter)
        #print self.textview1.get_all()
        self.window.set_default_size(300,200)
        self.window.show();
    

    
class Handler:
    def onDeleteWindow(self, *args):
        Gtk.main_quit(*args)

    def onButtonPressed(self, *args):
        print "Hello World!", args
           
if __name__ == "__main__":
    try:
        a = dwkMain()
        Gtk.main()
    except KeyboardInterrupt:
        pass