#!/usr/bin/env python
# -*- coding: utf-8 -*-
from bottle import route,run,get,template,request,static_file,TEMPLATE_PATH
import bottle
import os
import textile
import urlparse

global currentpath

EXT = '.dw'
WIKI_PATH = os.path.join(os.getcwd() , '../test/')
MEDIA_ROOT = os.path.join(os.getcwd() , '../dwk/img/')
RES_ROOT = os.path.join(os.getcwd() , '../dwk/resources/')

bottle.TEMPLATE_PATH.insert(0,  os.path.join(os.getcwd() , '../dwk/views/'))
bottle.debug(mode=True)


@get('/media/:path#.+#')
def server_static(path):
    return static_file(path, root=MEDIA_ROOT)

@get('/res/:path#.+#')
def server_static(path):
    return static_file(path, root=RES_ROOT)

@route('/')
def main():
    return getcontent('index')

@get('/favicon.ico')
def get_favicon():
    return static_file('favicon.ico', root=os.getcwd()) #server_static('favicon.ico')

@route('/<name>')
def hello(name):
    print name
    return getcontent(name)

def getcontent(filename):
    html = 'Create new page'
    file = open(os.path.join(WIKI_PATH , filename + EXT), 'r')
    content = textile.textile(file.read()).encode("utf-8")
    html = template('index', content=content, title='this is the title')
    return html

run(host='localhost', port=8333, debug=True)

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
