		//create the Tree control
		var oTree = new sap.ui.commons.Tree("tree");
		oTree.setTitle("Explorer");
		oTree.setWidth("100%");
		oTree.setHeight("auto");
		oTree.setShowHorizontalScrollbar(false);
		oTree.setShowHeaderIcons(true);
		//create Tree Nodes
		var oNode1 = new sap.ui.commons.TreeNode("node1", {text:"Computer", expanded: true});
		var oNode2 = new sap.ui.commons.TreeNode("node2", {text:"OSDisk (C:)", expanded: true});
		var oNode3 = new sap.ui.commons.TreeNode("node3", {text:"Program Files"});
		var oNode4 = new sap.ui.commons.TreeNode("node4", {text:"Windows"});
		var oNode5 = new sap.ui.commons.TreeNode("node5", {text:"Mass Storage (USB)"});
		var oNode6 = new sap.ui.commons.TreeNode("node6", {text:"Network"});

		oNode1.addNode(oNode2);
		oNode1.addNode(oNode5);

		oNode2.addNode(oNode3);
		oNode2.addNode(oNode4);

		//add Tree Node root to the Tree
		oTree.addNode(oNode1);
		oTree.addNode(oNode6);
		
		
		//create a vertical Splitter
		var oSplitterV = new sap.ui.commons.Splitter("splitterV"); 
		oSplitterV.setSplitterOrientation(sap.ui.commons.Orientation.vertical);
		oSplitterV.setSplitterPosition("15%");
		oSplitterV.setMinSizeFirstPane("10%");
		oSplitterV.setMinSizeSecondPane("30%");
		oSplitterV.setWidth("100%");
		//oSplitterV.setHeight("250px");

		//adding Labels to both panes

		oSplitterV.addFirstPaneContent(oTree);	
		var oLabel2 = new sap.ui.commons.Label({text: "Second Pane"});
		oSplitterV.addSecondPaneContent(oLabel2);		

		oSplitterV.placeAt("main");
		
