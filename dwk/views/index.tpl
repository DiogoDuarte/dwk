<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-GB">
<head>
<title>{{title}}</title>
<meta http-equiv="Content-Type"
	content="application/xhtml+xml; charset=utf-8" />
<meta name="description" content="Description of the web" />
<meta name="keywords" content="These are the keywords" />
<meta name="robots" content="index, follow" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="/res/css/index.css"
	media="screen" />
</head>
<body>

	<div id="header">

		<h1>The Perfect 'Left Menu' 2 Column Liquid Layout (Percentage
			widths)</h1>
		<h2>No CSS hacks. SEO friendly. No Images. No JavaScript.
			Cross-browser &amp; iPhone compatible.</h2>
		<ul>
			<li><a href="#">Selection 1 <span>Holy Grail</span></a></li>
			<li><a href="#">Selection 2 <span>Holy Grail</span></a></li>
			<li><a href="#">Selection 3 <span>Holy Grail</span></a></li>
			<li><a href="#">Selection 4 <span>Holy Grail</span></a></li>

			
		</ul>
		<p id="layoutdims">Measure columns in:</p>
	</div>
	<div class="colmask leftmenu">
		<div class="colleft">
			<div class="col1">
				<!-- Column 1 start -->
				{{content}}

				<!-- Column 1 end -->
			</div>
			<div class="col2">
				<!-- Column 2 start -->

				<div class="css-treeview">
					<ul>
						<li><input type="checkbox" id="item-0" checked="checked" /><label for="item-0">This
								Folder is Closed By Default</label>
							<ul>
								<li><input type="checkbox" id="item-0-0" /><label
									for="item-0-0">Ooops! A Nested Folder</label>
									<ul>
										<li><a href="./">Item 1</a></li>
										<li><a href="./">Item 2</a></li>
										<li><a href="./">Item 3</a></li>
									</ul></li>
							</ul></li>
					</ul>
				</div>

				<!-- Column 2 end -->
			</div>
		</div>
	</div>
	<div id="footer">
		<p>This page uses the Perfect 'Left Menu' 2 Column Liquid Layout</p>
	</div>

</body>
</html>