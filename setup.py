#!/usr/bin/env python


from distutils.core import setup


setup(name='dwk',
      version='0.0.1',
      description='This personal wiki for use with some VCS system (git,hg,bzr...).',
      author='Diogo Duarte',
      author_email='diogocarvalhoduarte@gmail.com',
      url='http://diogocduarte.com',
      platforms = ['any'],
      license = ['BSD'],
      long_description = """
        This personal wiki for use with some VCS system (git,hg,bzr...).
        """
      )